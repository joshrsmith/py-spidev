This project contains a python module for interfacing with SPI devices from user space via the spidev linux kernel driver.

This is a modified version of the code originally found [here](http://elk.informatik.fh-augsburg.de/da/da-49/trees/pyap7k/lang/py-spi)

It was then imported from [here](https://github.com/doceme/py-spidev)

Example use:

    import spidev
    dev = spidev.SpiDev(0,0) # open up /dev/spidev0.0
    dev.mode = 0
    dev.max_speed_hz = 500000
    dev.cshigh = False
    dev.lsbfirst = False
    dev.threewire = False
    dev.loop = False # loop is "loopback"
    dev.bits_per_word = 8
    #exchange 32 bits:
    values_in = [0x0, 0x0, 0x0, 0x0]
    values_out = dev.xfer(values_in)
    dev.close()
